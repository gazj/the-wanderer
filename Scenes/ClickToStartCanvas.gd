extends CanvasLayer

onready var is_clicked = false

signal clicked

func _ready():
	if OS.get_model_name() == "GenericDevice":
		$Overlay/RichTextLabel.bbcode_text = "click to play"
	else: $Overlay/RichTextLabel.bbcode_text = "tap to play"

func _input(e):
	if is_clicked: return
	if e is InputEventMouseButton and e.pressed and e.button_index == 1:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		emit_signal("clicked")
		is_clicked = true
