extends AudioStreamPlayer3D

signal stepped

func _on_Footsteps_finished():
	emit_signal("stepped")
	var rng = RandomNumberGenerator.new()
	pitch_scale = rng.randf_range(0.6, 1.2)
	play()
