extends Spatial

export var listen_text: String
onready var player_nearby = false

func _ready():
	# Begin movement animation (traveling animation is handled elsewhere)
	var animation = $AnimationPlayer.get_animation("Action001")
	animation.set_loop(true)
	$AnimationPlayer.play("Action001")
	
	# Connect to signal from Footstep sound player
	$Footsteps.connect("stepped", $Whistles, "on_Footsteps_stepped")

func die():
	#stop animation
	var animation = $AnimationPlayer.get_animation("Action001")
	animation.loop = false
	$AnimationPlayer.stop()
	
	#stop footstep sound
	$Footsteps.stop()
	
	
