extends AudioStreamPlayer3D

export var low_note: AudioStream
export var mid_note: AudioStream
export var high_note: AudioStream

# track steps in pairs
onready var other_step = false

func on_Footsteps_stepped():
	other_step = !other_step
	if other_step: return
	var tempo = 150.0
	var beat_length = 60.0 / tempo
	stream = mid_note
	play()
	yield(get_tree().create_timer(3 * beat_length), "timeout")
	stream = high_note
	play()
	yield(get_tree().create_timer(1 * beat_length), "timeout")
	stream = low_note
	play()
	yield(get_tree().create_timer(2 * beat_length), "timeout")
	stream = mid_note
	play()
	yield(get_tree().create_timer(2 * beat_length), "timeout")
	
