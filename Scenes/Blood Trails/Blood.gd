extends Spatial

onready var enabled = false

func _ready():
	hide_blood()

func enable():
	enabled = true

func disable():
	enabled = false

func show_blood():
	if enabled:
		show()

func hide_blood():
	hide()
