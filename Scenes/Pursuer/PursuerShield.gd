extends Area

onready var enabled = true

func _on_PursuerShield_area_entered(pursuer):
	if enabled:
		pursuer.die()
