extends Spatial

const PURSUER = preload("res://Scenes/Pursuer/Pursuer.tscn")

var time_since_last_spawn

onready var instances = 0
onready var enabled = true

export var max_instances = 3
export var time_between_spawns = 60

signal spawned(pursuer)

func _ready():
	# spawn immediately on first _process() call
	time_since_last_spawn = time_between_spawns

func _process(delta):
	time_since_last_spawn += delta
	if time_since_last_spawn >= time_between_spawns \
		and instances < max_instances and enabled:
		_spawn()

func _spawn():
	instances += 1
	time_since_last_spawn = 0
	var pursuer = PURSUER.instance()
	add_child(pursuer)
	pursuer.connect("died", self, "_on_Pursuer_died")
	emit_signal("spawned", pursuer)

func _on_Pursuer_died():
	instances -= 1
