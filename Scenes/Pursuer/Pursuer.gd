extends Area

export var speed = 500

onready var time_since_spawned = 0
onready var already_died = false

signal reached_player(pursuer)
signal died

# https://godotengine.org/qa/46057/how-to-get-the-class-name-of-a-custom-node
func get_class():
	return "Pursuer"

func chase(player, delta):
	global_transform.origin = global_transform.origin.move_toward(
		player.global_transform.origin,
		speed * delta
	)

func inflate_and_die():
	# this is somewhat timed to the overlay fade_out animation
	$Tween.interpolate_property($CPUParticles, 'mesh:size', Vector3(1, 1, 1),
		Vector3(5, 5, 5), 8, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_completed")
	die()

func die():
	# only call this method once
	if already_died: return
	already_died = true
	$PlayerSensor.queue_free()
	$Tween.interpolate_property($CPUParticles, 'mesh:size', Vector3(1, 1, 1),
		Vector3.ZERO, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.interpolate_property($PursueAudioPlayer, 'unit_db', 2,
		-20, 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	$Tween.start()
	yield($Tween, "tween_all_completed")
	queue_free()
	emit_signal("died")

func _on_PlayerSensor_body_entered(_body):
	emit_signal("reached_player", self)
