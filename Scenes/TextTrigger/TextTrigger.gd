extends Area

export var text: String

onready var player_entered = false

signal player_entered
signal player_exited

func _ready():
	_clear_text()

# Public method to update text and display changes immediately if player is nearby.
func set_text(_text: String):
	self.text = _text
	if player_entered:
		_display_text()

func _display_text():
	$Label.text = self.text

func _clear_text():
	$Label.text = ""

func _on_TextTrigger_body_entered(_body):
	player_entered = true
	emit_signal("player_entered")
	_display_text()

func _on_TextTrigger_body_exited(_body):
	player_entered = false
	emit_signal("player_exited")
	_clear_text()
