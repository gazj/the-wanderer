extends Spatial

export (String, MULTILINE) var good_text
export (String, MULTILINE) var bad_text
export (String, MULTILINE) var confused_text
export (String, MULTILINE) var music_text
export (String, MULTILINE) var wrong_place_text
