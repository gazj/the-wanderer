extends Spatial

export var click_sound: AudioStream
export var burn_sound: AudioStream
export var ignite_sound: AudioStream

var start_temp = 70
var small_fire_temp = 130
var target_temp = 200
var heat_rate = 9.6
var cool_rate = 1.2

onready var already_ignited = false
onready var ignition_method: String
onready var current_temp = start_temp
onready var player_nearby = false

var last_delta: float

signal ignited

func _ready():
	extinguish()
	$"Sitting Skeleton".hide()

func _process(delta):
	last_delta = delta
	_cool_off(delta)
	_update_small_fire()
	_update_pursuer_shield()
	_play_heating_sound()
	if not already_ignited:
		_check_combustion()

func heat_by_flashlight():
	current_temp += last_delta * heat_rate

func _cool_off(delta):
	current_temp = max(start_temp, current_temp - (delta * cool_rate))

func _update_small_fire():
	$SmallFire.stop()
	if current_temp > small_fire_temp and not already_ignited:
		$SmallFire.start()

func _update_pursuer_shield():
	$PursuerShield.enabled = $SmallFire/Light.visible or $Fire/Light.visible

func _play_heating_sound():
	if current_temp == start_temp or already_ignited:
		$HeatingAmbientSound.stop()
		return
	if not $HeatingAmbientSound.playing:
		$HeatingAmbientSound.play()
	# Sync temp progress with ambient sound volume level (toward 0db)
	var progress = (current_temp - start_temp) / (target_temp - start_temp)
	var db = -50 + (progress / .02)
	db = min(db, 0)
	$HeatingAmbientSound.volume_db = db

func _check_combustion():
	if current_temp >= target_temp:
		current_temp = target_temp #keep from going over
		ignite("flashlight")

func ignite(method = "unknown"):
	if already_ignited: return
	$Fire.start()
	emit_signal("ignited")
	already_ignited = true
	ignition_method = method
	$FireSound.stream = ignite_sound
	$FireSound.play()
	yield($FireSound, "finished")
	$FireSound.stream = burn_sound
	$FireSound.play()

func play_click():
	if current_temp < small_fire_temp and not already_ignited:
		$FireSound.stream = click_sound
		$FireSound.play()

func stop_click():
	if $FireSound.stream == click_sound:
		$FireSound.stop()

func extinguish():
	$Fire.stop()

func _on_PlayerSensor_body_entered(_body):
	player_nearby = true

func _on_PlayerSensor_body_exited(_body):
	player_nearby = false
