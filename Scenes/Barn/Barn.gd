extends Spatial

onready var flames = get_tree().get_nodes_in_group("barn_flames")

signal player_entered
signal player_exited
signal player_approached

func _ready():
	hide_body()

func ignite():
	for flame in flames:
		flame.start()

func extinguish():
	for flame in flames:
		flame.stop()

func show_body():
	$Skeleton.show()

func hide_body():
	$Skeleton.hide()

func _on_InsidePlayerSensor_body_entered(_body):
	emit_signal("player_entered")

func _on_InsidePlayerSensor_body_exited(_body):
	emit_signal("player_exited")

func _on_OutsidePlayerSensor_body_entered(_body):
	emit_signal("player_approached")
