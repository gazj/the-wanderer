extends Position3D

export(int, "barn", "campfire", "wanderer", "forest", "gate", "cemetery") var warp_to

signal triggered(location)

# https://docs.godotengine.org/en/stable/classes/class_%40globalscope.html#enum-globalscope-keylist
func _input(event):
	if !OS.is_debug_build(): return
	# Scancode of NUMPAD 0 + warp_to int maps NUMPAD keys to locations here
	if event is InputEventKey and event.scancode == 16777350 + warp_to:
		emit_signal("triggered", global_transform)

