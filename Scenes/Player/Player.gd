extends KinematicBody

enum State {IDLE, RUN, JUMP, FALL}

const JUMP_SPEED = 7
const JUMP_FRAMES = 1
const HOP_FRAMES = 3

export var mouse_look_sens = Vector2(.1, .1)
export var analog_look_sens = Vector2(8.0, 8.0)
export var move_speed = 10
export var acceleration = .5
export var gravity = -10
export var friction = 1.15
export var max_climb_angle = .6
export var max_look_down_angle = 55
export var max_look_up_angle = 155
export var boost_accumulation_speed = 1
export var max_boost_multiplier = 2
export (Array, AudioStream) var footstep_sounds
export var flashlight_on_sound: AudioStream
export var flashlight_off_sound: AudioStream
export var flashlight_cutout_sound: AudioStream
var analog_look_vector: Vector2
var playing_footstep_sound = false
var whistle_pattern: Array
var current_charge = 1 # charge of flashlight battery
var charge_cutout = 0.1 # cut out when flashlight battery falls below this
var charge_min_to_use = 0.3 # allow turning flashlight on if charge is above this
var charge_usage_rate = .035 # burn battery at this rate * delta
var charge_rate = 0.025 # charge battery at this rate * delta

onready var allow_input = false
onready var can_move = false
onready var has_moved = false
onready var can_toggle_flashlight = true
onready var flashlight_cutting_out = false
onready var light_source = null
onready var shortrange_raycast = $UpperCollider/Camera/Flashlight/light/ShortRangeRayCast
onready var midrange_raycast = $UpperCollider/Camera/Flashlight/light/MidRangeRayCast

signal heating_campfire
signal lit_pursuer(pursuer)
signal whistle_attempted
signal whistle_succeeded
signal made_first_move
signal lights_on
signal lights_off

# Called when the node enters the scene tree for the first time.
func _ready():
	$Tween.connect("tween_all_completed", self, "_on_tween_all_completed")
	reset_flashlight_charge()
	hide_lights()

func _process(delta):
	_update_flashlight_charge(delta)
	_check_shortrange_raycast()
	_check_midrange_raycast()
	if analog_look_vector:
		look(analog_look_vector, analog_look_sens)

func _check_shortrange_raycast():
	if shortrange_raycast.is_colliding():
		var collider = shortrange_raycast.get_collider().name
		if collider == "Campfire" and flashlight_is_on():
			emit_signal("heating_campfire")

func _check_midrange_raycast():
	if midrange_raycast.is_colliding():
		var collider = midrange_raycast.get_collider()
		if collider and collider.get_class() == "Pursuer" and flashlight_is_on():
			emit_signal("lit_pursuer", collider)

func _physics_process(delta):
	_process_input(delta)
	_process_movement(delta)

func reset_flashlight_charge():
	current_charge = 1

func _update_flashlight_charge(delta):
	if flashlight_is_on():
		# handle draining
		current_charge -= charge_usage_rate * delta
		current_charge = max(current_charge, 0)
		if current_charge < charge_cutout:
			cut_out_flashlight()
	else:
		# handle charging
		if light_source != null and light_source.light_energy > 0:
			current_charge += charge_rate * delta
			current_charge = min(current_charge, 1)

# Handles other input
func _input(event):
	if not allow_input: return
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED: return
	# Mouse movement
	if event is InputEventMouseMotion:
		look(event.relative, mouse_look_sens)

func look(look_vec: Vector2, sens: Vector2):
	rotate_y(deg2rad(look_vec.x * sens.y * -1))
	$UpperCollider/Camera.rotate_x(deg2rad(look_vec.y * sens.x * -1))
	
	var camera_rot = $UpperCollider/Camera.rotation_degrees
	camera_rot.x = clamp(camera_rot.x, max_look_down_angle, max_look_up_angle)
	$UpperCollider/Camera.rotation_degrees = camera_rot

var inbetween = false
func _on_tween_all_completed():
	inbetween = false
	crouch_floor = false

var state = State.FALL
var on_floor = false
var frames = 0
var crouching = false
var crouch_floor = false #true if started crouching on the floor
var input_dir = Vector3(0, 0, 0)
func _process_input(delta):
	if not allow_input: return
	
	# Jump
#	if Input.is_action_pressed("jump") && on_floor && state != State.FALL && (frames == 0 || frames > JUMP_FRAMES + 1):
#		frames = 0
#		state = State.JUMP

	# Whistle
	_handle_whistle_input(delta)
	
	# Handle analog look input
	_handle_analog_look_input(delta)
	
	# Flashlight
	if can_toggle_flashlight and Input.is_action_just_pressed("flashlight"):
		if flashlight_is_on():
			hide_lights(true)
		else:
			if current_charge >= charge_min_to_use:
				show_lights(true)
			else:
				# click, but do nothing
				$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.stream = flashlight_on_sound
				$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.play()
	
	# Crouch
	if Input.is_action_just_pressed("crouch"):
		if on_floor:
			crouch_floor = true
		crouching = true
		$Tween.interpolate_property($LowerCollider, "translation", 
				Vector3(0, -.25, 0), Vector3(0,.25, 0), .1, Tween.TRANS_LINEAR)
		$Tween.start()
		inbetween = true
		
	if Input.is_action_just_released("crouch"):
		crouching = false
		$Tween.interpolate_property($LowerCollider, "translation", 
				Vector3(0, .25, 0), Vector3(0, -.25, 0), .1, Tween.TRANS_LINEAR)
		$Tween.start()
		inbetween = true
	
	# WASD
	input_dir = Vector3(Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left"), 
			0,
			Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")).normalized()

var collision : KinematicCollision  # Stores the collision from move_and_collide
var velocity := Vector3(0, 0, 0)
var rotation_buf = rotation  # used to calculate rotation delta for air strafing
var turn_boost = 1
func _process_movement(delta):
	if not allow_input: return
	# state management
	if !collision:
		on_floor = false
		if state != State.JUMP:
			state = State.FALL
	else:
		if state == State.JUMP:
			pass
		elif Vector3.UP.dot(collision.normal) < max_climb_angle:
			state = State.FALL
		else:
			on_floor = true
			if input_dir.length() > .1 && (frames > JUMP_FRAMES+HOP_FRAMES || frames == 0):
				state = State.RUN
				turn_boost = 1
			else:
				state = State.IDLE
	
	#jump state
	if state == State.JUMP && frames < JUMP_FRAMES:
		velocity.y = JUMP_SPEED
		frames += 1 * delta * 60
	elif state == State.JUMP:
		state = State.FALL

	#fall state
	if state == State.FALL:
		if inbetween && crouching && crouch_floor:
			velocity.y = gravity;
		if velocity.y > gravity:
			velocity.y += gravity * delta * 4
	
	#run state
	if state == State.RUN:
		velocity += input_dir.rotated(Vector3(0, 1, 0), rotation.y) * acceleration
		if Vector2(velocity.x, velocity.z).length() > (move_speed/2 if crouching else move_speed):
			velocity = velocity.normalized() * (move_speed/2 if crouching else move_speed)
		velocity.y = ((Vector3(velocity.x, 0, velocity.z).dot(collision.normal)) * -1)
		
		# fake gravity to keep character on the ground
		# increase if player is falling down slopes instead of running
		velocity.y -= .0001 + (int(velocity.y < 0) * 1.1)  
		
	#idle state
	if state == State.IDLE && frames < HOP_FRAMES + JUMP_FRAMES:
		frames += 1 * delta * 60
	elif state == State.IDLE:
		turn_boost = 1
		if velocity.length() > .5:
			velocity /= friction
			velocity.y = ((Vector3(velocity.x, 0, velocity.z).dot(collision.normal)) * -1) - .0001

	#air strafe
	if state > 2:
		#x axis movement
		var rotation_d = rotation - rotation_buf
		if input_dir.x > .1 && rotation_d.y < 0:
			velocity = velocity.rotated(Vector3.UP, rotation_d.y )
			turn_boost += boost_accumulation_speed * delta 
		elif input_dir.x < -.1 && rotation_d.y > 0:
			velocity = velocity.rotated(Vector3.UP, rotation_d.y ) 
			turn_boost += boost_accumulation_speed * delta 
		
		if abs(input_dir.x) < .1 && on_floor:
			#z axis movement
			var movement_vector = Vector3(0,0,input_dir.z).rotated(Vector3(0, 1, 0), rotation.y) * move_speed /2
			if movement_vector.length() < .1:
				velocity = velocity
			elif Vector2(velocity.x, velocity.z).length() < move_speed:
				var xy = Vector2(movement_vector.x , movement_vector.z).normalized()
				velocity += Vector3(xy.x, 0, xy.y) * acceleration
				
		turn_boost = clamp(turn_boost, 1, max_boost_multiplier)
		rotation_buf = rotation

	#apply
	if !can_move: return
	if velocity.length() >= .5 || inbetween:
		collision = move_and_collide(velocity * Vector3(turn_boost, 1, turn_boost) * delta)
	else:
		velocity = Vector3(0, velocity.y, 0)
	if collision:
		if Vector3.UP.dot(collision.normal) < .5:
			velocity.y += delta * gravity
			clamp(velocity.y, gravity, 9999)
			velocity = velocity.slide(collision.normal).normalized() * velocity.length()
		elif turn_boost > 1.01:
			velocity = Vector3(velocity.x, velocity.y + ((Vector3(velocity.x, 0, velocity.z).dot(collision.normal)) * - 2) , velocity.z)
		else:
			velocity = velocity
	
	if velocity.x != 0 and velocity.y != 0:
		if !has_moved:
			emit_signal("made_first_move")
			has_moved = true
		if !playing_footstep_sound:
			playing_footstep_sound = true
			randomize()
			var sound = footstep_sounds[ rand_range(0, footstep_sounds.size() - 1) ]
			$FootstepsAudioPlayer.stream = sound
			var pitch_scale = 1
			var step_delay = 0.5
			if crouching:
				pitch_scale = 0.7
				step_delay = 0.7
			$FootstepsAudioPlayer.pitch_scale = pitch_scale
			$FootstepsAudioPlayer.play()
			yield(get_tree().create_timer(step_delay), "timeout")
			playing_footstep_sound = false

# Joystick movement captured here and applied elsewhere.
func _handle_analog_look_input(delta):
	analog_look_vector = Vector2.ZERO
	if Input.is_action_pressed("analog_look_left") \
		or Input.is_action_pressed("analog_look_right") \
		or Input.is_action_pressed("analog_look_up") \
		or Input.is_action_pressed("analog_look_down"):
		analog_look_vector = Input.get_vector("analog_look_left", "analog_look_right", "analog_look_up", "analog_look_down")

func _handle_whistle_input(delta):
	if not allow_input: return
	# pressed
	if Input.is_action_just_pressed("whistle_low"):
		emit_signal("whistle_attempted")
		$LowWhistleAudioPlayer.play()
		$MidWhistleAudioPlayer.stop()
		$HighWhistleAudioPlayer.stop()
		whistle_pattern.push_front({ "pitch": "low", "length": 0.0 })
	if Input.is_action_just_pressed("whistle_mid"):
		emit_signal("whistle_attempted")
		$LowWhistleAudioPlayer.stop()
		$MidWhistleAudioPlayer.play()
		$HighWhistleAudioPlayer.stop()
		whistle_pattern.push_front({ "pitch": "mid", "length": 0.0 })
	if Input.is_action_just_pressed("whistle_high"):
		emit_signal("whistle_attempted")
		$LowWhistleAudioPlayer.stop()
		$MidWhistleAudioPlayer.stop()
		$HighWhistleAudioPlayer.play()
		whistle_pattern.push_front({ "pitch": "high", "length": 0.0 })
	# held (checked by which audio player is playing)
	if $LowWhistleAudioPlayer.playing and whistle_pattern[0].pitch == "low":
		whistle_pattern[0].length += delta
	elif $MidWhistleAudioPlayer.playing and whistle_pattern[0].pitch == "mid":
		whistle_pattern[0].length += delta
	elif $HighWhistleAudioPlayer.playing and whistle_pattern[0].pitch == "high":
		whistle_pattern[0].length += delta
	# released
	if Input.is_action_just_released("whistle_low"):
		$LowWhistleAudioPlayer.stop()
	if Input.is_action_just_released("whistle_mid"):
		$MidWhistleAudioPlayer.stop()
		_check_whistle_pattern()
	if Input.is_action_just_released("whistle_high"):
		$HighWhistleAudioPlayer.stop()

func _check_whistle_pattern():
	var wp = whistle_pattern # for brevity
	if wp.size() < 4: return
	if _check_whistle_note(wp[3], "mid", 3) \
		and _check_whistle_note(wp[2], "high", 1) \
		and _check_whistle_note(wp[1], "low", 2) \
		and _check_whistle_note(wp[0], "mid", 2) \
		and not flashlight_is_on():
			emit_signal("whistle_succeeded")

func _check_whistle_note(note, pitch, beats):
	var tempo = 150.0 #bpm
	var beat_length = 60.0 / tempo
	var err = 0.5
	var min_length = float(beats) * beat_length * (1 - err)
	var max_length = float(beats) * beat_length * (1 + err)
	return note.pitch == pitch and note.length >= min_length and note.length <= max_length

func _set_audio_reverb(room_size: float):
	#footsteps
	var footsteps_bus_index = AudioServer.get_bus_index("Footsteps (Player)")
	var footsteps_reverb: AudioEffectReverb
	footsteps_reverb = AudioServer.get_bus_effect(footsteps_bus_index, 0)
	footsteps_reverb.room_size = room_size
	#whistling
	var whistling_bus_index = AudioServer.get_bus_index("Whistling (Player)")
	var whistling_reverb: AudioEffectReverb
	whistling_reverb = AudioServer.get_bus_effect(whistling_bus_index, 0)
	whistling_reverb.room_size = room_size

func flashlight_is_on():
	return $UpperCollider/Camera/Flashlight/light.visible
	
func show_lights(with_sound = false):
	if with_sound:
		$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.stream = flashlight_on_sound
		$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.play()
	$UpperCollider/Camera/Flashlight/light.show()
	$body_light.show()
	emit_signal("lights_on")

func hide_lights(with_sound = false):
	if with_sound:
		$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.stream = flashlight_off_sound
		$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.play()
	$UpperCollider/Camera/Flashlight/light.hide()
	$body_light.hide()
	emit_signal("lights_off")

func cut_out_flashlight():
	if not flashlight_is_on() or flashlight_cutting_out: return
	flashlight_cutting_out = true
	$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.stream = flashlight_cutout_sound
	$UpperCollider/Camera/Flashlight/FlashlightAudioPlayer.play()
	yield($UpperCollider/Camera/Flashlight/FlashlightAudioPlayer, "finished")
	$UpperCollider/Camera/Flashlight/light.hide()
	$body_light.hide()
	emit_signal("lights_off")
	flashlight_cutting_out = false

func set_light_source(source):
	light_source = source
		
func on_entered_barn():
	_set_audio_reverb(1)

func on_exited_barn():
	_set_audio_reverb(0)
