extends StaticBody

export var min_light_energy = 1.8
export var max_light_energy = 2.2
export var flicker_intensity = 0.025

func _ready():
	stop()

func _process(_delta):
	var intensity = flicker_intensity
	if randi() % 2 == 0:
		intensity = -intensity
	$Light.light_energy = clamp(
		$Light.light_energy + intensity,
		min_light_energy,
		max_light_energy
	)

func start():
	$Light.show()
	$Flame.emitting = true
	$Smoke.emitting = true
	$CollisionShape.disabled = false
	
func stop():
	$Light.hide()
	$Flame.emitting = false
	$Smoke.emitting = false
	$CollisionShape.disabled = true
