extends CanvasLayer

onready var player_has_moved = false

signal shown
signal hidden

func _ready():
	$Overlay/Label.text = ProjectSettings.get_setting("application/config/version")

func _process(_delta):
#	if ["HTML5"].has(OS.get_name()) and Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
#		_show()
	pass

func _input(e):
	if e is InputEventMouseButton and e.pressed and e.button_index == 1:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		_hide()
	elif e is InputEventKey and e.scancode == 16777217 and e.pressed:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		_show()

func _show():
	if player_has_moved and not $Overlay.visible:
		$Overlay.show()
		emit_signal("shown")

func _hide():
	if $Overlay.visible:
		$Overlay.hide()
		emit_signal("hidden")
