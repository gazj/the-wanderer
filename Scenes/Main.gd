extends Spatial

export var static_fade_in: AudioStream
export var static_short_burst: AudioStream

var good_ending_activated = false

onready var innerViewport = $ViewportContainer/Viewport
onready var outerViewport = innerViewport.get_node("ViewportContainer/Viewport")
onready var click_to_start_canvas = outerViewport.get_node("ClickToStartCanvas")
onready var title_canvas = outerViewport.get_node("TitleCanvas")
onready var controls_canvas = outerViewport.get_node("ControlsCanvas")
onready var wanderer = outerViewport.get_node("Wanderer")
onready var player = outerViewport.get_node("Player")
onready var barn = outerViewport.get_node("Barn")
onready var campfire = outerViewport.get_node("Campfire")
onready var skeleton_grave = outerViewport.get_node("Cemetery/Tombstones/SkeletonGrave")
onready var theme_music = outerViewport.get_node("ThemeMusic")
onready var static_player = outerViewport.get_node("Static")
onready var overlay_animation = outerViewport.get_node("OverlayCanvas/AnimationPlayer")

func _ready():
	title_canvas.get_node("Label").hide()
	click_to_start_canvas.get_node("Overlay").show()
	_init_game()
	click_to_start_canvas.get_node("Overlay/RichTextLabel").show()

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") and OS.is_debug_build():
		restart_game()
	_pursuers_chase_player(delta)

func _init_game():
	outerViewport.get_node("Credits/Camera").global_transform.origin.y = -100
	_load_dark_environment()
	_move_player_to_start()
	_connect_signals()
	_start_animations()
	_dim_whistle_lamps()

func _start_game():
	click_to_start_canvas.get_node("Overlay").hide()
	title_canvas.get_node("Label").show()
	theme_music.play()
	if !OS.has_feature("editor"): # skip intro animation during dev
		overlay_animation.set_current_animation("fade_in")
		yield(overlay_animation, "animation_finished")
	disable_blood()
	player.can_move = true
	player.allow_input = true

func restart_game():
	get_tree().change_scene("res://Scenes/Main.tscn")

func _connect_signals():
	controls_canvas.connect("shown", self, "_on_ControlsCanvas_shown")
	controls_canvas.connect("hidden", self, "_on_ControlsCanvas_hidden")
	player.connect("made_first_move", self, "_on_Player_made_first_move")
	player.connect("lit_pursuer", self, "_on_Player_lit_pursuer")
	player.connect("heating_campfire", self, "_on_Player_heating_campfire")
	player.connect("whistle_attempted", self, "_on_Player_whistle_attempted")
	player.connect("whistle_succeeded", self, "_on_Player_whistle_succeeded")
	player.connect("lights_on", self, "_on_Player_lights_on")
	player.connect("lights_off", self, "_on_Player_lights_off")
	barn.connect("player_entered", player, "on_entered_barn")
	barn.connect("player_exited", player, "on_exited_barn")
	barn.connect("player_approached", self, "_on_Player_approached_barn")
	campfire.connect("ignited", self, "_on_Campfire_ignited")
	skeleton_grave.get_node("Epitaph").connect("player_entered",
		self, "_on_Player_entered_skeleton_grave")
	for warp_point in get_tree().get_nodes_in_group("debug_warp_points"):
		warp_point.connect("triggered", self, "_on_Player_triggered_debug_warp")
	for warp_trigger in get_tree().get_nodes_in_group("forest_warp_triggers"):
		warp_trigger.connect("triggered", self, "_on_Player_triggered_forest_warp")
	for spawner in get_tree().get_nodes_in_group("pursuer_spawners"):
		spawner.connect("spawned", self, "_on_PursuerSpawner_spawned")
	for lamp in get_tree().get_nodes_in_group("lamps"):
		lamp.connect("player_entered", self, "_on_Player_entered_lamp", [lamp])
		lamp.connect("player_exited", self, "_on_Player_exited_lamp", [lamp])

func _load_dark_environment():
	var env = preload("res://runtime_env.tres")
	outerViewport.get_node("WorldEnvironment").environment = env

func _move_player_to_start():
	player.global_transform = outerViewport.get_node("PlayerStart").global_transform

func _move_player_to_campfire():
	player.global_transform = outerViewport.get_node("CampfireVision").global_transform

func _start_animations():
	# Wanderer
	var route_animation_player = wanderer.get_node("RouteAnimationPlayer")
	var roam_animation = route_animation_player.get_animation("roam")
	roam_animation.set_loop(true)
	route_animation_player.seek(0)
	route_animation_player.play("roam")

func _dim_whistle_lamps():
	for lamp in get_tree().get_nodes_in_group("whistle_lamps"):
		lamp.get_node("OmniLight").light_energy = 0

func _pursuers_chase_player(delta):
	for pursuer in get_tree().get_nodes_in_group("pursuers"):
		pursuer.chase(player, delta)

func enable_blood():
	var blood_nodes = get_tree().get_nodes_in_group("blood")
	for blood in blood_nodes:
		blood.enable()

func disable_blood():
	var blood_nodes = get_tree().get_nodes_in_group("blood")
	for blood in blood_nodes:
		blood.disable()

func show_blood():
	var blood_nodes = get_tree().get_nodes_in_group("blood")
	for blood in blood_nodes:
		blood.show_blood()

func hide_blood():
	var blood_nodes = get_tree().get_nodes_in_group("blood")
	for blood in blood_nodes:
		blood.hide_blood()

func check_good_ending():
	return campfire.ignition_method == "flashlight"

func _set_spawners_enabled(enabled):
	for spawner in get_tree().get_nodes_in_group("pursuer_spawners"):
			spawner.enabled = enabled

func _kill_all_pursuers():
	for pursuer in get_tree().get_nodes_in_group("pursuers"):
		pursuer.die()

func _on_Player_heating_campfire():
	campfire.heat_by_flashlight()

# Keep campfire from igniting by flashlight while attempting whistle method
func _on_Player_whistle_attempted():
	campfire.current_temp = campfire.start_temp
	for lamp in get_tree().get_nodes_in_group("whistle_lamps"):
		lamp.get_node("OmniLight").light_energy = 4
		yield(get_tree().create_timer(3), "timeout")
		lamp.get_node("OmniLight").light_energy = 0

func _on_Player_whistle_succeeded():
	if campfire.player_nearby:
		campfire.ignite("whistle")

func _on_Campfire_ignited():
	if campfire.already_ignited: return
	outerViewport.get_node("Barn").show_body()
	enable_blood()
	if not player.flashlight_is_on():
		show_blood()
	outerViewport.get_node("Cemetery/Gate").open()

func _on_Player_made_first_move():
	title_canvas.get_node("Label").hide()
	theme_music.stop()
	wanderer.get_node("Footsteps").play()
	controls_canvas.player_has_moved = true

func _on_Player_lit_pursuer(pursuer):
	pursuer.die()

func _on_Player_entered_skeleton_grave():
	_kill_all_pursuers()
	if not good_ending_activated and check_good_ending():
		#good end
		player.can_move = false
		skeleton_grave.get_node("Epitaph").set_text(skeleton_grave.good_text)
		yield(get_tree().create_timer(6), "timeout")
		overlay_animation.set_current_animation("fade_out")
		barn.ignite()
		barn.hide_body()
		wanderer.die()
		yield(overlay_animation, "animation_finished")
		_move_player_to_campfire()
		enable_blood()
		overlay_animation.set_current_animation("fade_in")
		yield(overlay_animation, "animation_finished")
		player.can_move = true
		good_ending_activated = true
	else:
		# bad text (or confused if campfire was skipped)
		player.can_move = false
		var text = skeleton_grave.bad_text
		if theme_music.playing:
			text = skeleton_grave.music_text
		elif good_ending_activated:
			text = skeleton_grave.wrong_place_text
		elif not campfire.already_ignited:
			text = skeleton_grave.confused_text
		skeleton_grave.get_node("Epitaph").set_text(text)
		yield(get_tree().create_timer(6), "timeout")
		overlay_animation.set_current_animation("fade_out")
		yield(overlay_animation, "animation_finished")
		restart_game()

func _on_Player_triggered_debug_warp(location):
	player.global_transform = location

func _on_Player_triggered_forest_warp():
	player.can_toggle_flashlight = false
	player.reset_flashlight_charge()
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	static_player.stream = static_fade_in
	static_player.play()
	yield(static_player, "finished")
	player.can_move = false
	#flicker
	for n in rng.randi_range(2, 3):
		player.hide_lights()
		yield(get_tree().create_timer(rng.randf_range(.03, .1)),"timeout")
		player.show_lights()
		static_player.stream = static_short_burst
		static_player.play()
		yield(get_tree().create_timer(rng.randf_range(.05, .2)),"timeout")
		static_player.stop()
	#campfire vision
	player.hide_lights()
	yield(get_tree().create_timer(rng.randf_range(.03, .1)),"timeout")
	player.show_lights()
	_move_player_to_campfire()
	# go no further if good ending is reached
	if check_good_ending():
		player.can_toggle_flashlight = true
		player.can_move = true
		return
	campfire.get_node("Fire").start()
	campfire.get_node("Sitting Skeleton").show()
	static_player.stream = static_short_burst
	static_player.play()
	yield(get_tree().create_timer(2),"timeout")
	static_player.stop()
	player.hide_lights()
	campfire.get_node("Fire").stop()
	campfire.get_node("Sitting Skeleton").hide()
	#back to start
	_move_player_to_start()
	player.show_lights()
	player.can_toggle_flashlight = true
	player.can_move = true
	
func _on_Player_approached_barn():
	if good_ending_activated:
		player.can_move = false
		player.hide()
		campfire.ignite()
		barn.ignite()
		barn.hide_body()
		enable_blood()
		wanderer.die()
		_set_spawners_enabled(false)
		_kill_all_pursuers()
		outerViewport.get_node("Credits").roll()

func _on_Player_lights_on():
	campfire.stop_click()
	hide_blood()
	for book in get_tree().get_nodes_in_group("books"):
		book.light_off()

func _on_Player_lights_off():
	campfire.play_click()
	show_blood()
	for book in get_tree().get_nodes_in_group("books"):
		book.light_on()

func _on_PursuerSpawner_spawned(pursuer):
	pursuer.connect("reached_player", self, "_on_Pursuer_reached_player")

func _on_Pursuer_reached_player(pursuer):
	player.hide_lights()
	player.allow_input = false
	pursuer.inflate_and_die()
	overlay_animation.set_current_animation("fade_out")
	yield(overlay_animation, "animation_finished")
	_move_player_to_start()
	overlay_animation.set_current_animation("fade_in")
	yield(overlay_animation, "animation_finished")
	player.allow_input = true
	player.show_lights()

func _on_Player_entered_lamp(lamp):
	player.set_light_source(lamp.get_node("OmniLight"))

func _on_Player_exited_lamp(_lamp):
	player.set_light_source(null)

func _on_ControlsCanvas_shown():
	player.allow_input = false

func _on_ControlsCanvas_hidden():
	player.allow_input = true
