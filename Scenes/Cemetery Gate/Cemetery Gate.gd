extends Spatial

func _ready():
	close()

func open():
	
	# Show only the open gate
	$closed.hide()
	$open.show()
	
	# Enable collision only on the open gate
	$closed.get_node("gateR/static_collision/shape0").disabled = true
	$open.get_node("gateR/static_collision/shape0").disabled = false

func close():
	# Show only the closed gate
	$open.hide()
	$closed.show()
	
	# Enable collision only on the closed gate
	$open.get_node("gateR/static_collision/shape0").disabled = true
	$closed.get_node("gateR/static_collision/shape0").disabled = false
	
