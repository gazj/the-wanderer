extends Area

signal triggered

func _on_Forest_Warp_Trigger_body_entered(_body):
	emit_signal("triggered")
