extends AudioStreamPlayer

export var light: AudioStream
export var heavy: AudioStream

onready var should_play_heavy = false

func _ready():
	play_next()

func play_next():
	stream = light
	if should_play_heavy:
		stream = heavy
		should_play_heavy = false
	play()
	yield(self, "finished")
	play_next()

func _on_HeavyAmbienceTrigger_body_entered(_body):
	should_play_heavy = true
