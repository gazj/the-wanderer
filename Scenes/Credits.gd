extends CanvasLayer

func roll():
	$Camera.current = true
	$Overlay.show()
	$RichTextLabel.show()
	$Camera/Light.hide()
	$AnimationPlayer.set_current_animation("barn")
	$AnimationPlayer.seek(0)
	$AnimationPlayer.play()
	yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer.set_current_animation("campfire")
	$AnimationPlayer.seek(0)
	$AnimationPlayer.play()
	yield($AnimationPlayer, "animation_finished")
	$Camera/Light.show()
	$AnimationPlayer.set_current_animation("cemetery")
	$AnimationPlayer.seek(0)
	$AnimationPlayer.play()
	yield($AnimationPlayer, "animation_finished")
	roll()
	
