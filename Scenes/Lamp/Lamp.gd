extends Spatial

signal player_entered
signal player_exited

func _process(_delta):
	$PursuerShield.enabled = $OmniLight.light_energy > 0

func _on_PlayerSensor_body_entered(_body):
	emit_signal("player_entered")

func _on_PlayerSensor_body_exited(_body):
	emit_signal("player_exited")
