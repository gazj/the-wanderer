# The Wanderer

A spooky game for Halloween 2021.

## Premise

The story takes place on an old farm in the woods (do farms belong in the woods?). It is your mission to explore the property, collect clues, and unravel the mystery of what happened leading up to the present. Can you use the information gathered to reach the "good" ending?

## Play Online

The game can be downloaded or played on the web at [Itch.io](https://gazjambo.itch.io/the-wanderer).

## Contributing

Pull requests are welcome, but this project isn't in active development past Halloween.

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html)
